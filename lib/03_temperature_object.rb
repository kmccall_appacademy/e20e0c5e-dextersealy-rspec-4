class Temperature

  def initialize(hash)
    @celsius = hash[:c] ? hash[:c] : (hash[:f] - 32) * 5.0 / 9.0
  end

  def in_fahrenheit
    (@celsius * 9.0 / 5.0) + 32
  end

  def in_celsius
    @celsius
  end

  def Temperature.from_celsius(temp)
    Temperature.new(:c => temp)
  end

  def Temperature.from_fahrenheit(temp)
    Temperature.new(:f => temp)
  end
end

class Celsius < Temperature
  def initialize(temp)
    super(:c => temp)
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    super(:f => temp)
  end
end
