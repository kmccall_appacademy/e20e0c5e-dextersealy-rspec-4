class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(word)
    if word.is_a?(Hash)
      entries[word.keys[0]] = word.values[0]
    else
      entries[word] = nil
    end
  end

  def keywords
    entries.keys.sort
  end

  def include?(word)
    entries.has_key?(word)
  end

  def find(word)
    result = {}
    for key, value in entries
      result[key] = value if key.start_with?(word)
    end
    result
  end

  def printable
    result = []
    for key in keywords
      result << "[#{key}] \"#{entries[key]}\""
    end
    result.join("\n")
  end

end
