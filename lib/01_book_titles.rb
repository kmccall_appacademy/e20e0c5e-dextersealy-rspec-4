class Book
  attr_reader :title

  def title=(newTitle)
    @title=capitalize(newTitle)
  end

  private

  def capitalize(title)
    exceptions = ["and", "the", "a", "an", "in", "of"]

    result = []
    for word in title.split()
      if result.count > 0 and exceptions.include?(word)
        result << word
      else
        result << word.capitalize
      end
    end

    result.join(" ")
  end
end
